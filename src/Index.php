<?php

namespace Mdt\Zxchain;

class Index
{

    private $url = 'https://open.zxchain.qq.com';  //官网url
 
    private $secretId = '';

    private $secretKey = '';

    //平台公钥
    private $platformPubKey = '';

    //平台私钥
    private $platformPriKey = '';

    //请求头
    private $header = [
        'Content-Type' => 'application/json;charset=utf-8',
        'Secret-Id' => '',
        'Signature-Time' => '',
        'Signature' => '',
        'Nonce' => '',
        'Cloud-trace-id' => ''
    ];

    //sdk包路由地址
    private $sdk_url = '';


    public function __construct()
    {
        if(!$this->secretId) {
            throw new \Exception('secretId is not null');
        }

        if(!$this->secretKey) {
            throw new \Exception('secretKey is not null');
        }

        if(!$this->platformPubKey) {
            throw new \Exception('platformPubKey is not null');
        }

        if(!$this->platformPriKey) {
            throw new \Exception('platformPriKey is not null');
        }

        if(!$this->sdk_url) {
            throw new \Exception('sdk_url is not null');
        }

        $this->header['Secret-Id'] = $this->secretId;
        $this->getSign();
    }

    /**
     * 获取请求头参数
     * User: chenpeiyu
     * Date: 2022/9/22
     * Time: 5:03 PM
     */
    function getSign()
    {
        $res = self::curl_post_request($this->sdk_url . '/generateApiSign', [
            'secretId' => $this->secretId,
            'secretKey' => $this->secretKey
        ]);

        $res = json_decode($res, true);
        if($res['err']) {
            throw new \Exception("签名生成失败");
        }

        $this->header['Signature'] = $res['signData']['Signature'];
        $this->header['Signature-Time'] = $res['signData']['SignatureTime'];
        $this->header['Nonce'] = $res['signData']['Nonce'];
        $this->header['Cloud-trace-id'] = date('YmdHis');
    }

    /**
     * sdk-生成助记词
     * User: chenpeiyu
     * Date: 2022/9/22
     * Time: 5:06 PM
     */
    function createMnemonic()
    {
        $res = self::curl_post_request($this->sdk_url . '/createMnemonic');

        $res = json_decode($res, true);
        if($res['err']) {
            throw new \Exception("助记词生成失败");
        }
        return $res['mnemonic'];
    }

    /**
     * sdk-生成子公私钥对
     * User: chenpeiyu
     * Date: 2022/9/29
     * Time: 4:16 PM
     */
    public function getderiveKeyPair($mnemonic)
    {
        $index = rand(1,12);
        $res = self::curl_post_request($this->sdk_url . '/deriveKeyPair', [
            'Mnemonic' => $mnemonic,
            'index' => $index
        ]);

        $res = json_decode($res, true);
        if($res['err']) {
            throw new \Exception("生成子公私钥对失败");
        }

        $res['index'] = $index;
        return $res;
    }

    /**
     * sign加密
     * User: chenpeiyu
     * Date: 2022/9/22
     * Time: 5:03 PM
     * @params:data:需要加密参数
     * @params:prikey:私钥（个人/平台）
     */
    public function signByPrikey($data, $PriKey)
    {

        $res = self::curl_get_request($this->sdk_url . '/signByPriKey', [
            'PriKey' => $PriKey,
            'Data' => $data
        ]);

        $res = json_decode($res, true);
        if($res['err']) {
            throw new \Exception("验签失败");
        }

        return $res['signedData'];
    }

    /**
     * 自然人注册实名
     * User: chenpeiyu
     * Date: 2022/10/12
     * Time: 1:54 PM
     * @personName:真实姓名
     * @email:邮箱
     * @mobile:手机号码
     * @idCard:身份证（必须和真实姓名配对上）
     * @cardType:证件类型 1-身份证 2-护照 3-港澳通行证 4-台湾通行证 5-外国人永居身份证 6-港澳台居民居住证 7-其他
     *
     * @return:userIdentification userIdentification:用户唯一标识
     */
    public function downGrade($personName, $email, $mobile, $idCard, $cardType)
    {
        $data = [
            'personName' => $personName,
            'email' => $email,
            'mobile' => $mobile,
            'idCard' => $idCard,
            'cardType' => $cardType,
            'platformPubKey' => $this->platformPubKey,
            'platformSignData' => $this->signByPrikey("{$personName}_{$mobile}_{$idCard}", $this->platformPriKey)
        ];

        $data = json_encode($data);
        $res = self::curl_post_request($this->url . '/api/v1/user/register/person_platform/down_grade', $data, 60, $this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }

        return $res['data']['userIdentification'];
    }

    /**
     * 受信平台用户身份绑定接口
     * User: chenpeiyu
     * Date: 2022/9/21
     * Time: 6:09 PM
     * @priKey:用户私钥
     * @pubKey:用户公钥
     * @userIdentification:用户唯一标识
     *
     * @return:address address:用户地址
     */
    public function submitByTrustedPlatform($priKey = "", $pubKey = "", $userIdentification)
    {
        $userSignData = $this->signByPrikey($userIdentification, $priKey);
        $data = [
            'userSignData' => $userSignData,
            'userPubKey' => $pubKey,
            'platformPubKey' => $this->platformPubKey,
            'platformSignData' => $this->signByPrikey($userSignData, $this->platformPriKey),
            'userIdentification' => $userIdentification
        ];

        $data = json_encode($data);
        $res = self::curl_post_request($this->url . '/api/v1/user/identity/bind/submit_by_trusted_platform', $data, 60, $this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }

        return $res['data']['address'];
    }

    /**
     * 绑定状态批量查询接口
     * User: chenpeiyu
     * Date: 2022/9/21
     * Time: 6:14 PM
     * @address:用户地址
     *
     * @return:data address:用户地址 status:1 绑定中 2 已绑定 3绑定失败 4 未发起过绑定（未查询到）
     */
    public function userBindQuery($address)
    {
        $url = $this->url . '/api/v1/user/identity/bind/query?';
        $url = $this->getUrlData($url, $address);
        $res = self::curl_get_request($url, [],$this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }

        //status 1 绑定中 2 已绑定 3绑定失败 4 未发起过绑定
        return $res['data'];
    }

    /**
     * 数字藏品 系列声明
     * User: chenpeiyu
     * Date: 2022/9/21
     * Time: 6:22 PM
     *
     * @priKey:用户私钥
     * @pubKey:用户公钥
     * @seriesName:系列名称
     * @totalCount:系列下数字藏品总个数，0表示没有限制
     * @operateId:请求ID，每个请求需要填唯一的id，重复请求用相同的id(一般就是以库的主键id)
     * @coverUrl:系列封面url，不超过1024个字符
     * @desc:系列描述信息，不超过500个字符
     * @maxPublishCount:历史遗留字段，无意义，填任意值都一样，建议填0即可。
     * @seriesBeginFromZero:系列下的数字藏品Id后缀，是否从0开始，true就是从0开始，默认为false，从1开始(1/0)
     *
     * @return:res taskId:任务id
     */
    public function claim($priKey, $pubKey, $seriesName, $totalCount=0, $operateId, $coverUrl, $desc, $maxPublishCount=0, $seriesBeginFromZero=false)
    {
        if($seriesBeginFromZero) {
            $seriesBeginFromZero_str = 'true';
        } else {
            $seriesBeginFromZero_str = 'false';
        }
        $signature = "{$this->platformPubKey}_{$pubKey}_series_claim_{$seriesName}_{$totalCount}_{$coverUrl}_{$desc}_{$maxPublishCount}_{$seriesBeginFromZero_str}_{$operateId}";
        $data = [
            'pubKey' => $pubKey,
            'platformPubKey' => $this->platformPubKey,
            'seriesName' => $seriesName,
            'totalCount' => $totalCount,
            'operateId' => $operateId,
            'coverUrl' => $coverUrl,
            'desc' => $desc,
            'maxPublishCount' => $maxPublishCount,
            'seriesBeginFromZero' => $seriesBeginFromZero,
            'signature' => $this->signByPrikey($signature, $priKey),
            'platformSignature' => $this->signByPrikey($signature, $this->platformPriKey)
        ];

        $data = json_encode($data);
        $res = self::curl_post_request($this->url . '/api/v1/nft/series/claim', $data, 60, $this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }

        return $res['data']['taskId'];
    }

    /**
     * 查询 数字藏品 系列声明结果
     * User: chenpeiyu
     * Date: 2022/9/21
     * Time: 6:32 PM
     * @taskId:任务id
     *
     * @return:res taskStatus:标记任务状态，任务执行中:2， 任务成功:7, 任务失败:10 seriesId:系列id txHash:交易hash chainTimestamp:链上交易时间戳
     */
    public function claimResult($taskId)
    {
        $data = [
            'platformPubKey' => urlencode($this->platformPubKey),
            'taskId' => $taskId
        ];

        $url = $this->url . '/api/v1/nft/series/claim/result?';
        $url = $this->getUrlData($url, $data);

        $res = self::curl_get_request($url, [], $this->header);

        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception(json_decode($res['retMsg']));

        }

        return $res['data'];
    }

    /**
     * 图片加密
     * User: chenpeiyu
     * Date: 2022/10/12
     * Time: 2:09 PM
     */
    public function sm3Hash($url)
    {
        $data = [
            'data' => base64_encode(file_get_contents($url))
        ];
        $data = json_encode($data);
        $res = self::curl_post_request($this->sdk_url . '/sm3Hash', $data, 60, $this->header);
        $res = json_decode($res, true);
        if($res['err']) {
            throw new \Exception($res['err']);
        }

        return $res['digest'];
    }

    /**
     * 发行 数字藏品
     * User: chenpeiyu
     * Date: 2022/10/12
     * Time: 2:10 PM
     * @pubKey:用户公钥
     * @priKey:用户私钥
     * @auther:作者,中文+英文（数字或符号为非法输入）（不超过30个字符）
     * @name:数字藏品名字，中英文数字均可，不超过256个字符
     * @url:介质url，不超过1024个字符
     * @displayUrl:预览图url，不超过1024个字符。（至信链浏览器展示预览图尺寸为290*290，请上传比例为1:1的图片）
     * @desc:数字藏品简介，500个字符以内
     * @flag:标签，【文创】【游戏】【动漫】······，30个字符以内
     * @publishCount:发行量，如果没有系列，就只能为1，如果有系列从1开始，比如如有100个，系列id范围则为[1-100]，单次发行个数不超过1000，同系列下同介质个数总共不能超过5000
     * @seriesId:系列id(没有填写"")
     * @seriesBeginIndex:系列子ID从多少开始，没有系列只能填1。 有系列情况下，根据系列声明时指定seriesBeginFromZero决定是否可以从0开始。总体上不超过系列的最大值，（比如系列如果从1开始，最大值为100，系列ID只能从1-100）
     * @sellStatus:1:可售 2:不可售
     * @sellCount:可售状态下有意义，表示售卖多少积分(没有为0)
     * @operateId:请求ID(建议使用mysql主键id)
     * @metaData:扩展字段，用户自定义，长度不超过1024个字符(没有为"")
     * @packageType:"0601": 元商品协议通用版 "0602"：元商品协议金融版
     *
     *
     * @return:taskId taskId:任务id
     *
     */
    public function nftPublish($pubKey, $priKey, $author, $name, $url, $displayUrl, $desc, $flag="", $publishCount=1, $seriesId="", $seriesBeginIndex=1, $sellStatus=2, $sellCount=0, $operateId, $metaData="", $packageType="0601")
    {
        $hash = $this->sm3Hash($url);
        $signature = "{$this->platformPubKey}_{$pubKey}_publish_nft_{$author}_{$name}_{$url}_{$displayUrl}_{$hash}_{$desc}_{$flag}_{$publishCount}_{$seriesId}_{$seriesBeginIndex}_{$sellStatus}_{$sellCount}_{$metaData}_{$operateId}";

        $data = [
            'pubKey' => $pubKey,
            'platformPubKey' => $this->platformPubKey,
            'author' => $author,
            'name' => $name,
            'url' => $url,
            'displayUrl' => $displayUrl,
            'hash' => $hash,
            'desc' => $desc,
            'flag' => $flag,
            'publishCount' => $publishCount,
            'seriesId' => $seriesId,
            'seriesBeginIndex' => $seriesBeginIndex,
            'sellStatus' => $sellStatus,
            'sellCount' => $sellCount,
            'operateId' => $operateId,
            'metaData' => $metaData,
            'signature' => $this->signByPrikey($signature, $priKey),
            'platformSignature' => $this->signByPrikey($signature, $this->platformPriKey),
            'packageType' => $packageType
        ];

        $data = json_encode($data);
        $res = self::curl_post_request($this->url . '/api/v1/nft/publish', $data, 60, $this->header);
        $res = json_decode($res, true);

        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }

        return $res['data']['taskId'];
    }

    /**
     * 查询 数字藏品 发行结果
     * User: chenpeiyu
     * Date: 2022/10/12
     * Time: 2:16 PM
     *
     * @taskId:任务id
     *
     * @return:res nftIdBegin:数字藏品Id格式，发行人公钥hash系列系列索引id，申请多少个，最后一段计算出来即可，比如申请10个,数字藏品IdBegin为xx_xx_1，那么就可以推导出x_xx_1到x_xx_10 taskStatus:标记任务状态，任务执行中:2， 任务成功:7, 任务失败:10 txHash:交易hash
     */
    public function resultPublish($taskId)
    {
        $data = [
            'platformPubKey' => urlencode($this->platformPubKey),
            'taskId' => $taskId
        ];

        $url = $this->url . '/api/v1/nft/publish/result?';
        $url = $this->getUrlData($url, $data);

        $res = self::curl_get_request($url, [], $this->header);

        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception(json_decode($res['retMsg']));
        }

        return $res['data'];
    }

    /**
     * 转移
     * User: chenpeiyu
     * Date: 2022/10/10
     * Time: 4:00 PM
     *
     * @pubKey:数字藏品可操作者的公钥
     * @priKey:数字藏品可操作者的私钥
     * @receiverAddr:数字藏品接收者的地址
     * @nftId:要转移的nftId
     * @operateId:请求ID，每个请求需要填唯一的id，重复请求用相同的id(建议使用Mysql主键id)
     *
     * @return:taskId taskId:任务id
     */
    public function transfer($pubKey, $priKey, $receiverAddr, $nftId, $operateId)
    {
        $signature = "{$pubKey}_{$receiverAddr}_nft_transfer_{$nftId}_{$operateId}";
        $data = [
            'pubKey' => $pubKey,
            'receiverAddr' => $receiverAddr,
            'nftId' => $nftId,
            'operateId' => $operateId,
            'signature' => $this->signByPrikey($signature, $priKey)
        ];

        $data = json_encode($data);
        $res = self::curl_post_request($this->url . '/api/v2/nft/transfer', $data, 60, $this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception($res['retMsg']);
        }
        return $res['data'];
    }

    /**
     * 转移获取结果
     * User: chenpeiyu
     * Date: 2022/10/10
     * Time: 4:01 PM
     *
     * @pubKey:数字藏品可操作者的公钥
     * @taskId:任务id
     *
     * @return:res taskStatus:标记任务状态，任务执行中:2， 任务成功:7, 任务失败:10 txHash:交易hash
     */
    public function transferResult($pubKey, $taskId)
    {
        $data = [
            'operatorPubKey' => urlencode($pubKey),
            'taskId' => $taskId
        ];

        $url = $this->url . '/api/v1/nft/transfer/result?';
        $url = $this->getUrlData($url, $data);

        $res = self::curl_get_request($url, [], $this->header);
        $res = json_decode($res, true);
        if($res['retCode'] != 0) {
            throw new \Exception(json_decode($res['retMsg']));

        }

        return $res['data'];
    }

    public function getUrlData($url, $data = [])
    {
        $url_data = [];
        foreach($data as $k => $v)
        {
            $url_data[] = "{$k}={$v}";
        }
        $url_data = implode('&', $url_data);
        return $url.$url_data;
    }

    public function curl_post_request($url,$data=null,$timeout=60, $headers=[])
    {
        $new_headers = [];
        if($headers) {
            foreach($headers as $k => $v)
            {
                $new_headers[] = $k.':'.$v;
            }
        }
        //curl验证成功
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//// 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);//// 跳过证书检查
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $new_headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            print curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    private function curl_get_request($url, $data, $headers=[])
    {
        $new_headers = [];
        if($headers) {
            foreach($headers as $k => $v)
            {
                $new_headers[] = $k.':'.$v;
            }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $new_headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        if($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);//结果是否显示出来，1不显示，0显示
        //判断是否https
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            print curl_error($ch);
        }
        curl_close($ch);
        return $data;
    }
}
